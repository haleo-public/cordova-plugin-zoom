# Zoom Ionic SDK for Ionic Angular

The plugin code is taken from Ionic SDK available on https://marketplace.zoom.us/.
The native code in `libs/` folder is taken from the individual native SDKs. 

## Documentation
Please visit [Zoom Ionic SDK](https://marketplace.zoom.us/docs/sdk/native-sdks/ionic/) to learn how to use the SDK and run the sample application.

